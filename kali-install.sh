#!/bin/bash

# set colors for use throughout the script.
RED="\033[01;31m"
GREEN="\033[01;32m"
YELLOW="\033[01;33m"
BLUE="\033[01;34m"
BOLD="\033[01;01m"
RESET="\033[00m"
export DEBIAN_FRONTEND=noninteractive

# set global install directory
installdir=/opt/

# check if root, if not, exit script.
if (( $EUID != 0 ))
then
    echo -e "\n[${RED}!${RESET}] you are ${RED}not${RESET} root"
    exit
fi

# check if running on debian, if not, exit.
if (lsb_release -d | grep -q "Kali" && lsb_release -c | grep -q "kali-rolling")
then
    echo ""
else
    echo -e "\n[${RED}!${RESET}] this is ${RED}not${RESET} Kali"
    exit
fi

# perform updates so we aren't doing it in every function
echo -e "\n[${GREEN}+${RESET}] performing ${YELLOW}updates${RESET}" && \
    apt-get -qq update && \
    apt-get -qq upgrade -y

# perform install of common tools so we aren't doing it in (almost) every function
echo -e "\n[${GREEN}+${RESET}] installing common ${YELLOW}system tools${RESET} and ${YELLOW}pipenv${RESET}" && \
    apt-get -qq install -y build-essential curl exploitdb firefox-esr git nano nmap netcat-traditional wget whois wireshark \ # system tools
	    python python3 python-dev python3-dev python3-pip \ # python tools
	    postgresql \ # database tools
	    tmux && \ # terminal tools
    pip3 install pipenv # python virtual environments

# tmux configuration improvements
echo -e "\n[${GREEN}+${RESET}] configuring ${YELLOW}tmux${RESET}" && \
    cat <<EOF > $HOME/.tmux.conf
set -g default-terminal "screen-256color" # colors!
setw -g xterm-keys on
set -s escape-time 10                     # faster command sequences
set -sg repeat-time 600                   # increase repeat timeout
set -s focus-events on

set -g prefix2 C-a                        # GNU-Screen compatible prefix
bind C-a send-prefix -2

set -q -g status-utf8 on                  # expect UTF-8 (tmux < 2.2)
setw -q -g utf8 on

set -g history-limit 500000                # boost history

# -- display -------------------------------------------------------------------

set -g base-index 1           # start windows numbering at 1
setw -g pane-base-index 1     # make pane numbering consistent with windows

setw -g automatic-rename on   # rename window to reflect current program
set -g renumber-windows on    # renumber windows when a window is closed

set -g set-titles on          # set terminal title

set -g display-panes-time 800 # slightly longer pane indicators display time
set -g display-time 1000      # slightly longer status messages display time

set -g status-interval 1      # redraw status line every 10 seconds

# clear both screen and history
bind -n C-l send-keys C-l \; run 'sleep 0.1' \; clear-history

# activity
set -g monitor-activity on
set -g visual-activity off

# -- navigation ----------------------------------------------------------------

# create session
bind C-c new-session

# find session
bind C-f command-prompt -p find-session 'switch-client -t %%'

# split current window horizontally
bind - split-window -v
# split current window vertically
bind _ split-window -h

# pane navigation
bind -r h select-pane -L  # move left
bind -r j select-pane -D  # move down
bind -r k select-pane -U  # move up
bind -r l select-pane -R  # move right
bind > swap-pane -D       # swap current pane with the next one
bind < swap-pane -U       # swap current pane with the previous one

# maximize current pane
bind + run 'cut -c3- ~/.tmux.conf | sh -s _maximize_pane "#{session_name}" #D'

# pane resizing
bind -r H resize-pane -L 2
bind -r J resize-pane -D 2
bind -r K resize-pane -U 2
bind -r L resize-pane -R 2

# window navigation
unbind n
unbind p
bind -r C-h previous-window # select previous window
bind -r C-l next-window     # select next window
bind Tab last-window        # move to last active window

# reload configuration
bind r source-file ~/.tmux.conf \; display '~/.tmux.conf sourced'

# -- windows & pane creation ---------------------------------------------------

# new window retains current path, possible values are:
#   - true
#   - false (default)
tmux_conf_new_window_retain_current_path=false

# new pane retains current path, possible values are:
#   - true (default)
#   - false
tmux_conf_new_pane_retain_current_path=true

# new pane tries to reconnect ssh sessions (experimental), possible values are:
#   - true
#   - false (default)
tmux_conf_new_pane_reconnect_ssh=false

# prompt for session name when creating a new session, possible values are:
#   - true
#   - false (default)
tmux_conf_new_session_prompt=false


# -- display -------------------------------------------------------------------

# RGB 24-bit colour support (tmux >= 2.2), possible values are:
#  - true
#  - false (default)
tmux_conf_theme_24b_colour=true

# window style
tmux_conf_theme_window_fg='default'
tmux_conf_theme_window_bg='default'

# highlight focused pane (tmux >= 2.1), possible values are:
#   - true
#   - false (default)
tmux_conf_theme_highlight_focused_pane=false

# focused pane colours:
tmux_conf_theme_focused_pane_fg='default'
tmux_conf_theme_focused_pane_bg='#0087d7'               # light blue

# pane border style, possible values are:
#   - thin (default)
#   - fat
tmux_conf_theme_pane_border_style=thin

# pane borders colours:
tmux_conf_theme_pane_border='#444444'                   # gray
tmux_conf_theme_pane_active_border='#00afff'            # light blue

# pane indicator colours
tmux_conf_theme_pane_indicator='#00afff'                # light blue
tmux_conf_theme_pane_active_indicator='#00afff'         # light blue

# status line style
tmux_conf_theme_message_fg='#000000'                    # black
tmux_conf_theme_message_bg='#f45a07'                    # orange # yellow #ffff00
tmux_conf_theme_message_attr='bold'

# status line command style (<prefix> : Escape)
tmux_conf_theme_message_command_fg='#ffff00'            # yellow
tmux_conf_theme_message_command_bg='#000000'            # black
tmux_conf_theme_message_command_attr='bold'

# window modes style
tmux_conf_theme_mode_fg='#000000'                       # black
tmux_conf_theme_mode_bg='#ffff00'                       # yellow
tmux_conf_theme_mode_attr='bold'

# status line style
tmux_conf_theme_status_fg='#8a8a8a'                     # light gray
tmux_conf_theme_status_bg='#080808'                     # dark gray
tmux_conf_theme_status_attr='none'

# terminal title
#   - built-in variables are:
#     - #{circled_window_index}
#     - #{circled_session_name}
#     - #{hostname}
#     - #{hostname_ssh}
#     - #{username}
#     - #{username_ssh}
tmux_conf_theme_terminal_title='#h #S ● #I #W'

# window status style
#   - built-in variables are:
#     - #{circled_window_index}
#     - #{circled_session_name}
#     - #{hostname}
#     - #{hostname_ssh}
#     - #{username}
#     - #{username_ssh}
tmux_conf_theme_window_status_fg='#8a8a8a'              # light gray
tmux_conf_theme_window_status_bg='#080808'              # dark gray
tmux_conf_theme_window_status_attr='none'
tmux_conf_theme_window_status_format='#I #W'
#tmux_conf_theme_window_status_format='#{circled_window_index} #W'
#tmux_conf_theme_window_status_format='#I #W#{?window_bell_flag,🔔,}#{?window_zoomed_flag,🔍,}'

# window current status style
#   - built-in variables are:
#     - #{circled_window_index}
#     - #{circled_session_name}
#     - #{hostname}
#     - #{hostname_ssh}
#     - #{username}
#     - #{username_ssh}
tmux_conf_theme_window_status_current_fg='#000000'      # black
tmux_conf_theme_window_status_current_bg='#36b9ff'      # light blue
tmux_conf_theme_window_status_current_attr='bold'
tmux_conf_theme_window_status_current_format='#I #W'
#tmux_conf_theme_window_status_current_format='#{circled_window_index} #W'
#tmux_conf_theme_window_status_current_format='#I #W#{?window_zoomed_flag,🔍,}'

# window activity status style
tmux_conf_theme_window_status_activity_fg='default'
tmux_conf_theme_window_status_activity_bg='default'
tmux_conf_theme_window_status_activity_attr='underscore'

# window bell status style
tmux_conf_theme_window_status_bell_fg='#ffff00'         # yellow
tmux_conf_theme_window_status_bell_bg='default'
tmux_conf_theme_window_status_bell_attr='blink,bold'

# window last status style
tmux_conf_theme_window_status_last_fg='#00afff'         # light blue
tmux_conf_theme_window_status_last_bg='default'
tmux_conf_theme_window_status_last_attr='none'

# status left/right sections separators
tmux_conf_theme_left_separator_main=''  #   /!\ you don't need to install Powerline
tmux_conf_theme_left_separator_sub=''   #   you only need fonts patched with
tmux_conf_theme_right_separator_main='' #   Powerline symbols or the standalone
tmux_conf_theme_right_separator_sub=''  #   PowerlineSymbols.otf font

# status left/right content:
#   - separate main sections with '|'
#   - separate subsections with ','
#   - built-in variables are:
#     - #{battery_bar}
#     - #{battery_hbar}
#     - #{battery_percentage}
#     - #{battery_status}
#     - #{battery_vbar}
#     - #{circled_session_name}
#     - #{hostname_ssh}
#     - #{hostname}
#     - #{loadavg}
#     - #{pairing}
#     - #{prefix}
#     - #{root}
#     - #{synchronized}
#     - #{uptime_d}
#     - #{uptime_h}
#     - #{uptime_m}
#     - #{uptime_s}
#     - #{username}
#     - #{username_ssh}
tmux_conf_theme_status_left=' #S '
tmux_conf_theme_status_right='#{prefix}#{pairing}#{synchronized} %l:%M:%S %p , %a %d %b | #{username}#{root} | #{hostname} '

# status left style
tmux_conf_theme_status_left_fg='#000000,#e4e4e4,#e4e4e4'  # black, white , white
tmux_conf_theme_status_left_bg='#ffff00,#00afff'  # yellow, white blue
tmux_conf_theme_status_left_attr='bold,none,none'

# status right style
tmux_conf_theme_status_right_fg='#8a8a8a,#e4e4e4,#000000' # light gray, white, black
tmux_conf_theme_status_right_bg='#080808,#d70000,#e4e4e4' # dark gray, red, white
tmux_conf_theme_status_right_attr='none,none,bold'

# pairing indicator
tmux_conf_theme_pairing='👓 '          # U+1F453
tmux_conf_theme_pairing_fg='none'
tmux_conf_theme_pairing_bg='none'
tmux_conf_theme_pairing_attr='none'

# prefix indicator
tmux_conf_theme_prefix='⌨ '            # U+2328
tmux_conf_theme_prefix_fg='none'
tmux_conf_theme_prefix_bg='none'
tmux_conf_theme_prefix_attr='none'

# root indicator
tmux_conf_theme_root='!'
tmux_conf_theme_root_fg='none'
tmux_conf_theme_root_bg='none'
tmux_conf_theme_root_attr='bold,blink'

# synchronized indicator
tmux_conf_theme_synchronized='🔒'     # U+1F512
tmux_conf_theme_synchronized_fg='none'
tmux_conf_theme_synchronized_bg='none'
tmux_conf_theme_synchronized_attr='none'

# clock style (when you hit <prefix> + t)
# you may want to use %I:%M %p in place of %R in tmux_conf_theme_status_right
tmux_conf_theme_clock_colour='#00afff'  # light blue
tmux_conf_theme_clock_style='12'

# -- clipboard -----------------------------------------------------------------

# in copy mode, copying selection also copies to the OS clipboard
#   - true
#   - false (default)
# on macOS, this requires installing reattach-to-user-namespace, see README.md
# on Linux, this requires xsel or xclip
tmux_conf_copy_to_os_clipboard=false
EOF

# enable ssh access.
enableSSH(){
    echo -e "\n[${GREEN}+${RESET}] changing SSH to ${RED}public key only${RESET}, enabling ssh access and configuring sshd_config" && \
	apt-get -qq install -y openssh-server && \
	cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak && \
#	sed -i "s/^#\PasswordAuthentication.*/PasswordAuthentication no/g" /etc/ssh/sshd_config && \
#	sed -i "s/^#\PermitRootLogin.*/PermitRootLogin no/g" /etc/ssh/sshd_config && \
	cat <<EOF > /etc/ssh/sshd_config
#Port 22
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Key Exchange Algorithms by order of preference
KexAlgorithms curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256

# Ciphers by order of preference
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr

# MACs by order of preference
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com

# Logging
#SyslogFacility AUTH
LogLevel VERBOSE

# Authentication:

#LoginGraceTime 2m
PermitRootLogin no
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

PubkeyAuthentication yes

# Expect .ssh/authorized_keys2 to be disregarded by default in future.
#AuthorizedKeysFile	.ssh/authorized_keys .ssh/authorized_keys2

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no
#PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM yes

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
PrintMotd no
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# Allow client to pass locale environment variables
AcceptEnv LANG LC_*

# override default of no subsystems
Subsystem	sftp	/usr/lib/openssh/sftp-server -f AUTHPRIV -l INFO

# Example of overriding settings on a per-user basis
#Match User anoncvs
#	X11Forwarding no
#	AllowTcpForwarding no
#	PermitTTY no
#	ForceCommand cvs server
EOF
	systemctl enable ssh && \
	systemctl restart ssh && \
	echo -e "\n[${YELLOW}!${RESET}] SSH is now ${RED}public key only with root login disabled${RESET}. Remember to add the ${YELLOW}authorized_keys${RESET} file under foresitelabs/.ssh"
}

# install ZSH because we like pretty colors and expanded functionality.
installZSH(){
    cd $HOME && \
	echo -e "\n[${GREEN}+${RESET}] configuring ${YELLOW}zsh${RESET}" && \
	apt-get -qq install -y zsh && \
	git clone --quiet --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto" && \
	ln -s $HOME/.zprezto/runcoms/zlogin $HOME/.zlogin && \
	ln -s $HOME/.zprezto/runcoms/zlogout $HOME/.zlogout && \
	ln -s $HOME/.zprezto/runcoms/zpreztorc $HOME/.zpreztorc && \
	ln -s $HOME/.zprezto/runcoms/zprofile $HOME/.zprofile && \
	ln -s $HOME/.zprezto/runcoms/zshenv $HOME/.zshenv && \
	ln -s $HOME/.zprezto/runcoms/zshrc $HOME/.zshrc && \
	chsh -s $(which zsh) && \
	echo "alias pyhttpd='python -m SimpleHTTPServer 8080'" >> $HOME/.zshrc

    for u in $(ls /home);
    do
	echo -e "echo -e 'USE ROOT \(sudo su\) !!'" >> /home/$u/.bashrc
    done
}

# install PowerSploit. This is a collection of useful pentest powershell modules.
powerSploit(){
    cd $installdir && \
	echo -e "\n[${GREEN}+${RESET}] downloading ${YELLOW}powersploit${RESET}" && \
	git clone https://github.com/PowerShellMafia/PowerSploit.git
}


# installs the auto_enumeration script created by Nick and JT. Does a good job of finding open ports and services.
autoenumInstall() {
    cd $installdir && \
	echo -e "\n[${GREEN}+${RESET}] downloading/installing ${YELLOW}auto_enum${RESET}" && \
	apt-get -qq install -y ike-scan dnsutils masscan nbtscan wfuzz && \
	git clone https://gitlab.com/foresitelabs/auto_enum.git
}

# installs CrackMapExecute. Useful post-exploitaion and situational awareness tool.
cmeInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}CrackMapExec${RESET}" && \
	apt-get -qq install -y crackmapexec
}

# installs Metasploit Framework and sets up the datbase. Standard pentesting framework.
metasploitInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Metasploit${RESET}" && \
	apt-get -qq install -y metasploit-framework
}

# installs Icebreaker. This is a useful tool that automates some top attacks. Includes installation of Empire, Deathstar, Responder, ridenum, john the ripper, scf attack.
# shouldn't need the libssl installs
iceBreakerInstall(){
    cd $installdir && \
	echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}icebreaker${RESET}" && \
	echo -e "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-jessie-prod jessie main" > /etc/apt/sources.list.d/microsoft.list && \
	echo -e "deb http://deb.debian.org/debian jessie main" > /etc/apt/sources.list.d/jessie.list && \
	curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
	apt-get -qq update && \
	apt-get -qq install -y \
		multiarch-support libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev swig3.0 python-m2crypto && \
	apt-get -qq install -y -t jessie libssl1.0.0 powershell && \
	git clone --recursive https://github.com/DanMcInerney/icebreaker && \
	cd icebreaker && \
	./setup.sh && \
	pipenv install --three && \
	pipenv install IPython # the developer has not added this to the install script, yet requires it.
}

## Icebreaker individual tools
##############################
# Ridenum
installRIDenum(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}RIDenum${RESET}" && \
	apt-get -qq install -y ridenum
}

# Powershell Empire
installEmpire(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Empire${RESET}" && \
	apt-get -qq install -y powershell-empire
}

# DeathStar
installDeathStar(){
    cd $installdir && \
	echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}DeathStar${RESET}" && \
	git clone https://github.com/byt3bl33d3r/DeathStar && \
	cd DeathStar && \
	pipenv install --three
}

# Impacket
installImpacket(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Impacket Scripts${RESET}" && \
	apt-get -qq install -y impacket-scripts
}

# Responder
installResponder(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Responder${RESET}" && \
	apt-get -qq install -y responder
}
##############################

# installs ssh-audit. Great tool for ssh validation.
sshAuditInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}ssh-audit${RESET}" && \
	apt-get -qq install -y ssh-audit
}

# installs rdp-sec-check. Great for validating RDP.
rdpSecCheckInstall(){
    cd $installdir && \
	echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}rdp-sec-check${RESET}" && \
	apt-get -qq install -y perl && \
	echo "yes" | cpan install Encoding::BER && \
	git clone https://github.com/portcullislabs/rdp-sec-check.git
}

# installs testssl.sh. Useful for validating SSL/TLS.
testsslInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}testssl.sh${RESET}" && \
	apt-get -qq install -y testssl.sh
}

# installs onesixtyone. Useful for SNMP validation.
onesixtyoneInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}onesixtyone${RESET}" && \
	apt-get -qq install -y onesixtyone
}

# installs Terminator. Brad wants it.
terminatorInstall(){
    cd $HOME && \
	echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}terminator${RESET}" && \
	apt-get -qq install -y terminator && \
	mkdir -p $HOME/.config/terminator/ && \
	cat <<EOF > $HOME/.config/terminator/config
[global_config]
  enabled_plugins = TerminalShot, LaunchpadCodeURLHandler, APTURLHandler, LaunchpadBugURLHandler
[keybindings]
[layouts]
  [[default]]
    [[[child1]]]
      parent = window0
      type = Terminal
    [[[window0]]]
      parent = ""
      type = Window
[plugins]
[profiles]
  [[default]]
    background_color = "#ffffff"
    background_darkness = 0.9
    copy_on_selection = True
    cursor_color = "#aaaaaa"
    foreground_color = "#000000"
    palette = "#000000:#000000:#000000:#000000:#000000:#000000:#000000:#000000:#000000:#000000:#000000:#000000:#000000:#000000:#000000:#000000"
    scrollback_infinite = True
    show_titlebar = False
EOF
}

# installs Remmina RDP client.
installRemmina(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}remmina${RESET}" && \
	apt-get -qq install -y remmina
}

# installs NFS tools for mounting network file shares.
installNFStools(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}nfs-common${RESET} and ${YELLOW}cifs-utils${RESET}" && \
	apt-get -qq install -y nfs-common cifs-utils
}

# installs an FTP client
installFTPStuff(){
    echo -e "\n[${GREEN}+${RESET}] installing an ${YELLOW}ftp${RESET} client" && \
	apt-get -qq install -y ftp
}

# installs Covenant C2 Framework
installCovenant(){
    cd $installdir && \
	echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Covenant${RESET}" && \
	apt-get -qq install -y \
		apt-transport-https ca-certificates gnupg2 software-properties-common && \
	curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
	add-apt-repository \
	     "deb [arch=amd64] https://download.docker.com/linux/debian \
	     $(lsb_release -cs) \
	     stable" && \
	apt-get -qq update && \
	apt-get -qq install -y docker-ce && \
	git clone --recurse-submodules https://github.com/cobbr/Covenant && \
	cd Covenant/Covenant && \
	docker build -t covenant . && \
	echo "covenant(){docker run -it -p 7443:7443 -p 80:80 -p 443:443 --name covenant -v /opt/Covenant/Covenant/Data:/app/Data covenant --username foresite --computername 0.0.0.0}" | tee -a $HOME/.zshrc && \
	echo -e "\n[${GREEN}+${RESET}] Usage:\nIn one terminal: ${YELLOW}covenant${RESET}\nFollow prompts."
}

# installs lsassy
installLsassy(){
    pip3 install lsassy
}

## MANUAL INSTALL TOOLS
#######################
# installs fuzzbunch. This is the NSA's windows attack tool.
fuzzbunchInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}fuzzbunch${RESET}" && \
	echo -e 'dpkg --add-architecture i386 && \
	apt-get update && \
	apt-get install -y \
		git \
		wine \
		wine32 \
		winbind && \
	wget -q https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks && \
	chmod +x ./winetricks && \
	WINEPREFIX="/opt/.wine-fuzzbunch" WINEARCH=win32 wine wineboot && \
	export WINEPREFIX=/opt/.wine-fuzzbunch && \
	wine cmd.exe /c "REG ADD HKCU\Environment\ /v PATH /t REG_SZ /d c:\\windows;c:\\windows\\system;C:\\Python26;C:\\fuzzbunch-debian\\windows\\fuzzbunch" && \
	git clone https://github.com/mdiazcl/fuzzbunch-debian.git $installdir/.wine-fuzzbunch/drive_c/fuzzbunch-debian && \
	winetricks python26 && \
	echo -e "export WINEPREFIX=/opt/.wine-fuzzbunch" >> $HOME/.zshrc && \
	echo -e "export WINEPREFIX=/opt/.wine-fuzzbunch" >> $HOME/.bashrc && \
	rm ./winetricks\n'
}

# installs Veil-Evasion for payload obfuscation
installVeil(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Veil${RESET}" && \
	apt-get -qq install veil
}
#######################

# display the menu and wait for a choice.
menu(){
    echo -e \
"ForesiteLABS Offensive Debian\n\n\
What you want to do?\n\n\
	0 Enable SSH\n\
	1 Install zprezto\n\
	2 Install auto_enumeration\n\
	3 Install CrackMapExec\n\
	4 Install PowerSploit\n\
	5 Install icebreaker\n\
	6 Install Metasploit Framework\n\
	7 Install ssh-audit\n\
	8 Install rdp-sec-check\n\
	9 Install testssl.sh\n\
	10 Install onesixtyone\n\
	11 Install Terminator\n\
	12 Install Remmina\n\
	13 Install nfs-tools\n\
	14 Install FTP client\n\
	15 Install RID enum\n\
	16 Install Empire\n\
	17 Install DeathStar\n\
	18 Install Impacket\n\
	19 Install Responder\n\
	20 Install Covenant\n\
	21 Install lsassy\n\
	22 Install Veil\n\
	23 Fuzzbunch Install Instructions\n\
	24 Install All\n\
	25 Exit\n"
}

if [ "$1" = "full" ]
   then
       enableSSH
       installZSH
       autoenumInstall
       cmeInstall
       powerSploit
       metasploitInstall
       iceBreakerInstall
       sshAuditInstall
       rdpSecCheckInstall
       testsslInstall
       onesixtyoneInstall
       terminatorInstall
       installNFStools
       installFTPStuff
       installRemmina
       installRIDenum
       installEmpire
       installDeathStar
       installImpacket
       installResponder
       installCovenant
       installLsassy
       installVeil
       fuzzbunchInstall
else
    while : ;
    do
	menu
	read -rp "Enter choice: " choice
	case $choice in
	    0)enableSSH;;
	    1)installZSH;;
	    2)autoenumInstall;;
	    3)cmeInstall;;
	    4)powerSploit;;
	    5)iceBreakerInstall;;
	    6)metasploitInstall;;
	    7)sshAuditInstall;;
	    8)rdpSecCheckInstall;;
	    9)testsslInstall;;
	    10)onesixtyoneInstall;;
	    11)terminatorInstall;;
	    12)installRemmina;;
	    13)installNFStools;;
	    14)installFTPStuff;;
	    15)installRIDenum;;
	    16)installEmpire;;
	    17)installDeathStar;;
	    18)installImpacket;;
	    19)installResponder;;
	    20)installCovenant;;
	    21)installLsassy;;
	    22)installVeil;;
	    23)fuzzbunchInstall;;
	    24)enableSSH && installZSH && autoenumInstall && cmeInstall && powerSploit && metasploitInstall && iceBreakerInstall && sshAuditInstall && rdpSecCheckInstall && testsslInstall && sslscanInstall && onesixtyoneInstall && terminatorInstall && installNfstools && installFTPStuff && installRemmina && installRIDenum && installEmpire && installDeathStar && installImpacket && installResponder && installLsassy && installVeil && fuzzbunchInstall;;
	    25)clear && break ;;
	    *) echo -e "\n[${RED}!${RESET}]Choice not in list" >&2; exit 2
	esac
    done
fi
